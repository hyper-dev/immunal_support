$(document).ready(function(){
	$("#featured > ul").tabs({fx:{opacity: "toggle"}}).tabs("rotate", 2500, true);
});

$(document).ready(function(){
	$("a").each(function(){
		if( $(this).attr('href') == '#'){
			if( $(this).hasClass('faq_menu')){
				$(this).colorbox({
					href: '/section_updating',
					onLoad: function() {
				    	//$('#cboxClose').remove();
						}
				});
			} else {
				if( !$(this).hasClass('close')){
					$(this).colorbox({
						href: '/under_construction',
						onLoad: function() {
					    	//$('#cboxClose').remove();
							}
					});
				}
			}
		}
	});
});

function shareW(link)
{
    window.open(link.href,'','toolbar=0,status=0,width=626,height=436');
    return false;
}

 
function changeFontSize(size) {  
    if($.cookie("fontSize")){currentSize = $.cookie("fontSize")}  
    else {currentSize = 13}  
	if(size == 2) {  
		newSize = 16;  
		$('.body').css('font-size', newSize + 'px');  
		$.cookie("fontSize", newSize)  
	}
    if(size == 1) {   
		newSize = 14;  
        $('.body').css('font-size', newSize + 'px');  
        $.cookie("fontSize", newSize)  
    }  
    if(size == 0) {  
 		newSize = 13;  
        $('.body').css('font-size', newSize + 'px');  
        $.cookie("fontSize", newSize)  
    }  
}  
$(document).ready(function(){
	$('.body').css('font-size',  $.cookie("fontSize") + 'px'); 
}); 